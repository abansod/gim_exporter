FROM python:3.7
COPY gim_exporter.py .
COPY sidecar.py .
COPY gim.py .
COPY gim_config_igui_sync_loop.py .
COPY influxdb_exporter.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "gim_exporter.py"]
